import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePassworkComponent } from './change-password.component';

describe('ChangePassworkComponent', () => {
  let component: ChangePassworkComponent;
  let fixture: ComponentFixture<ChangePassworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangePassworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangePassworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
