import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from 'src/app/common/api-service/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MustMatch } from 'src/app/common/validations/must-match.validator';
import { Md5 } from 'ts-md5';
import { Subscription } from 'rxjs/internal/Subscription';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePassworkComponent implements OnInit,OnDestroy {

  // validate
  form: FormGroup;

  //role
  staff = {
    id: '',
    email: '',
    role: '',
    password: '',
  };

  //value hide
  hide = true;
  hide1 = true;
  hide2 = true;

  //password
  passwordMdOld: any;
  passwordMdNew: any;

  //old password
  oldpassword: string;

  //new password
  newpassword: string;

  //subscription
  subscription: Subscription[] = [];

  //repassword
  repassword: string;

  /**
   * constructor
   * @param api
   * @param formBuilder
   * @param router
   */
  constructor(
    private api: ApiService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {

    //form validation
    this.form = this.formBuilder.group(
      {
        password: [null, [Validators.required, Validators.maxLength(50)]],
        newpassword: [
          null,
          [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(50),
          ],
        ],
        repassword: [
          null,
          [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(50),
          ],
        ],
      },

      //check newpassword == repassword
      { validator: MustMatch('newpassword', 'repassword') }
    );
  }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.staff = this.api.staffSubject.value;
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * on Change Password
   */
  onChangePassClick() {
    // return if error
    if (this.form.status != 'VALID') {
      this.api.showWarning('Vui lòng nhập các mục đánh dấu *');
      return;
    }

    //encode password
    this.passwordMdNew = Md5.hashAsciiStr(this.newpassword).toString();
    this.passwordMdOld = Md5.hashAsciiStr(this.oldpassword).toString();
    this.oldpassword = this.passwordMdOld;

    const param = {
      email: this.staff.email,
      password: this.passwordMdNew,
      id: this.staff.id,
    };

    //if password = password data
    if (this.oldpassword == this.api.staffSubject.value.password) {
      this.staff.password = this.newpassword;

      //update password
      this.subscription.push(
        this.api.excuteAllByWhat(param, '311').subscribe((data) => {
          if (data) {
            this.api.showSuccess('Cập nhật mật khẩu thành công ');
            this.form.reset();
          }
        })
      );
    } else {
      this.api.showError('Mật khẩu cũ chưa đúng hoặc mật khẩu mới chưa trùng nhau ');
    }
  }

  /**
   * on button Cancel
   */
  onBtnCancel() {
    this.form.reset();
  }
}
