import { Component, OnInit, Inject, ViewChild,OnDestroy } from '@angular/core';
import { ApiService } from '../../../common/api-service/api.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable, Observer, Subscription } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-b1user',
  templateUrl: './b1user.component.html',
  styleUrls: ['./b1user.component.scss'],
})
export class B1userComponent implements OnInit, OnDestroy {
  /** for table */
  displayedColumns: string[] = [
    'select',
    'name',
    'address',
    'sex',
    'class',
    'edit',
  ];

  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.position + 1
    }`;
  }
  // end for table;

  // data binding
  listClass: any;
  nameFind: string = '';
  name: string = '0';
  branches: any[] = [];

  //role
  staff = {
    email: '',
    role: '',
  };

  //data permission
  isPermissionMenu2: boolean = false;

  //subscription
  subscription: Subscription[] = [];

  /**
   * constructor
   * @param api
   * @param dialog
   */
  constructor(private api: ApiService, public dialog: MatDialog) {}

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.onLoadDataUser();
    this.getBranchClass();
    this.staff = this.api.staffSubject.value;
    this.onLoadPermission();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * onLoadPermission
   */
  onLoadPermission() {
    // load permission user = 3 (crud)
    if (this.staff.role.search('b:3') >= 0) {
      this.isPermissionMenu2 = true;
    }
  }

  /**
   * get data user
   */
  onLoadDataUser() {
    const param = {};
    this.subscription.push(
      this.api.excuteAllByWhat(param, '200').subscribe((data) => {
        if (data) {
          // set data for table
          this.dataSource = new MatTableDataSource(data);
        } else {
          this.dataSource = new MatTableDataSource([]);
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.selection = new SelectionModel<any>(true, []);
      })
    );
  }

  /**
   * get Branch Class
   */
  getBranchClass() {
    const param = {};
    this.subscription.push(
      this.api.excuteAllByWhat(param, '100').subscribe((data) => {
        this.branches = data;
      })
    );
  }

  /**
   * get Name Branches By Id
   */
  getNameBranchesById(id) {
    return this.branches.filter((e) => e.id == id)[0]?.name;
  }

  /**
   * on Convert sex
   */
  onConvertSex(sex) {
    if (sex == 1) {
      return 'Nam';
    }
    if (sex == 0) {
      return 'Nữ';
    }
  }

  /**
   * on Delete Click
   */
  onBtnDelClick() {
    // get listId selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach((item) => {
      if (listId == '') {
        listId += item.id;
      } else {
        listId += ',' + item.id;
      }
    });

    const param = { listid: listId };

    // start delete
    if (listId !== '') {
      this.subscription.push(
        this.api.excuteAllByWhat(param, '203').subscribe((data) => {
          // load data grid
          this.onLoadDataUser();

          //scroll top
          window.scroll({ left: 0, top: 0, behavior: 'smooth' });

          // show toast success
          this.api.showSuccess('Xóa thành công ');
        })
      );
    } else {
      // show toast warning
      this.api.showWarning('Vui lòng chọn 1 mục để xóa ');
    }
    this.selection = new SelectionModel<any>(true, []);
  }

  /**
   * on insert data
   * @param event
   */
  onInsertData() {
    const dialogRef = this.dialog.open(B1userDialog, {
      width: '700px',
      data: { type: 0, id: 0 },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.onLoadDataUser();
      }
    });
  }

  /**
   * on update data
   * @param event
   */
  onUpdateData(row) {
    const dialogRef = this.dialog.open(B1userDialog, {
      width: '700px',
      data: { type: 1, input: row },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.onLoadDataUser();
      }
    });
  }
}

/**
 * Component show thông tin để insert hoặc update
 */
@Component({
  selector: 'b1user-dialog',
  templateUrl: 'b1user-dialog.html',
  styleUrls: ['./b1user.component.scss'],
})
export class B1userDialog implements OnInit, OnDestroy {
  observable: Observable<any>;
  observer: Observer<any>;
  type: number;

  //subscription
  subscription: Subscription[] = [];

  // init input value
  input: any = {
    name: '',
    address: '',
    sex: '',
    idlop: '',
    role: '',
  };

  //form
  form: FormGroup;

  //sex
  sexs: any[] = [
    { value: '1', viewValue: 'Nam' },
    { value: '0', viewValue: 'Nữ' },
  ];

  /**
   * constructor
   * @param dialogRef
   * @param data
   * @param api
   * @param formBuilder
   */
  constructor(
    public dialogRef: MatDialogRef<B1userDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api: ApiService,
    private formBuilder: FormBuilder
  ) {
    this.type = data.type;

    // nếu là update
    if (this.type == 1) {
      this.input = data.input;
    }

    // add validate for controls
    this.form = this.formBuilder.group({
      name: [
        null,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(100),
        ],
      ],
      address: [
        null,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(255),
        ],
      ],
      sex: [null, [Validators.required]],
      idlop: [null, [Validators.required, Validators.maxLength(100)]],
    });

    // xử lý bất đồng bộ
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });
  }

  /**
   * Oninit
   */
  ngOnInit() {}

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
  }

  /**
   * on ok click
   */
  onBtnClick(): void {
    //if type = 0 insert else update
    this.subscription.push(
      this.api
        .excuteAllByWhat(this.input, '' + Number(201 + this.type) + '')
        .subscribe((data) => {
          this.dialogRef.close(true);
          this.api.showSuccess('Xử Lý Thành Công ');
        })
    );
  }
}
