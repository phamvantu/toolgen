import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../common/api-service/api.service';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-i-my-profile',
  templateUrl: './i-my-profile.component.html',
  styleUrls: ['./i-my-profile.component.scss'],
})
export class IMyProfileComponent implements OnInit,OnDestroy {

  //data binding
  staff = {
    id: '',
    email: '',
    name: '',
    passwork: '',
    img: '',
    created_date: '',
  };

  // validate
  form: FormGroup;

  /** for table */
  subscription: Subscription[] = [];

  // flag insert
  insertFlag: boolean = false;

  /**
   * constructor
   * @param api
   * @param formBuilder
   */
  constructor(private api: ApiService, private formBuilder: FormBuilder) {

    // add validate for controls
    this.form = this.formBuilder.group({
      name: [
        null,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(100),
        ],
      ],
      email: [
        null,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(255),
          Validators.email,
        ],
      ],
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.staff = this.api.staffSubject.value;
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * on update info staff
   */
  onBtnUpdateClick() {
    const param = {
      id: this.staff.id,
      email: this.staff.email,
      name: this.staff.name,
    };

    //update profile
    this.subscription.push(
      this.api.excuteAllByWhat(this.staff, '312').subscribe((data) => {
        if (data) {
          this.api.showSuccess('Cập nhật thành công ');
        }
      })
    );
  }

  /**
   * on update img
   */
  onUploadImgClick() {
    const param = {
      id: this.staff.id,
      img: this.staff.img,
    };

    //check validation
    if (this.form.status != 'VALID') {
      this.api.showWarning('Vui lòng nhập các mục đánh dấu * ');
      return;
    } else {

      //update img in profile
      this.subscription.push(
        this.api.excuteAllByWhat(param, '313').subscribe((data) => {
          if (data) {
            this.api.showSuccess('Cập nhật thành công ');
          }
        })
      );
    }
  }
}
