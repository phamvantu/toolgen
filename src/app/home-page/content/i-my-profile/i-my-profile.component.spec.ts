import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IMyProfileComponent } from './i-my-profile.component';

describe('IMyProfileComponent', () => {
  let component: IMyProfileComponent;
  let fixture: ComponentFixture<IMyProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IMyProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IMyProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
