import { Component, OnInit, Inject, ViewChild, OnDestroy } from '@angular/core';
import { ApiService } from '../../../common/api-service/api.service';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable, Observer, Subscription } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-c1staff',
  templateUrl: './c1staff.component.html',
  styleUrls: ['./c1staff.component.scss'],
})
export class C1staffComponent implements OnInit, OnDestroy {
  /** for table */
  displayedColumns: string[] = [
    'select',
    'name',
    'email',
    'created_date',
    'edit',
  ];

  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.position + 1
    }`;
  }
  // end for table;

  //data binding value
  input = {
    name: '',
    email: '',
    password: '',
    created_date: '',
    role: '',
  };

  //is permission menu
  isPerMissionMenu3: boolean = false;

  //staff list
  staffList: any[];

  //subscription
  subscription: Subscription[] = [];

  //role
  staff = {
    email: '',
    role: '',
  };

  /**
   * constructor
   * @param api
   * @param dialog
   */
  constructor(private api: ApiService, public dialog: MatDialog) {}

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.onLoadDataStaff();
    this.staff = this.api.staffSubject.value;
    this.onLoadPermission();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * on Load Permission
   */
  onLoadPermission() {
    // load permission
    if (this.staff.role.search('c:3') >= 0) {
      this.isPerMissionMenu3 = true;
    }
  }

  /**
   * get Data staff
   */
  onLoadDataStaff() {
    const param = {};
    //select all data staff
    this.subscription.push(
      this.api.excuteAllByWhat(param, '300').subscribe((data) => {
        if (data) {
          // set data for table
          this.staffList = data;
          this.dataSource = new MatTableDataSource(data);
        } else {
          this.dataSource = new MatTableDataSource([]);
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.selection = new SelectionModel<any>(true, []);
      })
    );
  }

  /**
   * on Delete Click
   */
  onBtnDelClick() {
    // get listId selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach((item) => {
      if (listId == '') {
        listId += item.id;
      } else {
        listId += ',' + item.id;
      }
    });

    const param = { listid: listId };

    // start delete
    if (listId !== '') {
      //delete staff id
      this.subscription.push(
        this.api.excuteAllByWhat(param, '303').subscribe((data) => {
          // load data staff
          this.onLoadDataStaff();

          //scroll top
          window.scroll({ left: 0, top: 0, behavior: 'smooth' });

          // show toast success
          this.api.showSuccess('Xóa thành công ');
        })
      );
    } else {
      // show toast warning
      this.api.showWarning('Vui lòng chọn 1 mục để xóa ');
    }
    this.selection = new SelectionModel<any>(true, []);
  }

  /**
   * on update data
   * @param event
   */
  onUpdateData(row) {
    const dialogRef = this.dialog.open(C1staffDialog, {
      width: '700px',
      data: { type: 1, input: row },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.onLoadDataStaff();
      }
    });
  }
}

/**
 * Component show thông tin để insert hoặc update
 */
@Component({
  selector: 'app-c1staff',
  templateUrl: 'c1staff-dialog.component.html',
  styleUrls: ['./c1staff.component.scss'],
})
export class C1staffDialog implements OnInit, OnDestroy {
  observable: Observable<any>;
  observer: Observer<any>;
  type: number;

  //subscription
  subscription: Subscription[] = [];

  // init input value
  input = {
    id: '',
    name: '',
    email: '',
    password: '',
    created_date: '',
    role: '',
  };
  date = new FormControl(moment());

  //sex value
  sexs: any[] = [
    { value: '1', viewValue: 'Nam' },
    { value: '0', viewValue: 'Nữ' },
  ];

  //list Permission
  listComponents = [
    {
      value: 'a',
      viewValue: 'Component lớp',
    },
    {
      value: 'b',
      viewValue: 'Component người dùng',
    },
    {
      value: 'c',
      viewValue: 'Component nhân viên',
    },
  ];

  //options permission
  options = [
    {
      value: '1',
      viewValue: 'Không',
    },
    {
      value: '2',
      viewValue: 'Chỉ xem',
    },
    {
      value: '3',
      viewValue: 'Quản trị',
    },
  ];

  //form
  form: FormGroup;

  // detail user
  detailUser: any;

  //list permission in array
  listPersAr: any[] = [];

  /**
   * constructor
   * @param dialogRef
   * @param data
   * @param api
   * @param formBuilder
   */
  constructor(
    public dialogRef: MatDialogRef<C1staffDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api: ApiService,
    private formBuilder: FormBuilder
  ) {
    this.type = data.type;

    // if type==1 -> update
    if (this.type == 1) {
      this.input = data.input;
    }

    // add validate for controls
    this.form = this.formBuilder.group({
      name: [
        null,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(100),
        ],
      ],
      email: [
        null,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(255),
          Validators.email,
        ],
      ],
      created_date: [null, [Validators.required]],
    });

    // xử lý bất đồng bộ
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.getDetailUser();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * get detail user
   */
  getDetailUser() {
    const param = { id: this.input.id };

    //select all data staff where id
    this.subscription.push(
      this.api.excuteAllByWhat(param, '310').subscribe((data) => {
        if (data.length > 0) {
          this.detailUser = data[0];
        }
        this.processedListPermission();
      })
    );
  }

  /**
   * processed List Permission to array
   */
  processedListPermission() {
    let listPermissionsAr = this.detailUser.role.split(',');
    listPermissionsAr.forEach((element) => {
      let subPerAr = element.split(':');
      this.listPersAr.push({ component: subPerAr[0], option: subPerAr[1] });
    });
  }

  /**
   * on Button Update Click
   */
  onBtnUpdateClick(): void {
    let listPermissionsStr = '';

    //handling update permission
    this.listPersAr.forEach((element) => {
      if (listPermissionsStr == '') {
        listPermissionsStr += element.component + ':' + element.option;
      } else {
        listPermissionsStr += ',' + element.component + ':' + element.option;
      }
    });

    //assign role = handling update permission
    this.input.role = listPermissionsStr;

    //update staff
    this.subscription.push(
      this.api.excuteAllByWhat(this.input, '302').subscribe((data) => {
        this.dialogRef.close(true);
        this.api.showSuccess('Xử Lý Thành Công ');
      })
    );
  }
}
