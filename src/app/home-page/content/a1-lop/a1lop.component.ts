import { Component, OnInit, Inject, ViewChild,OnDestroy } from '@angular/core';
import { ApiService } from '../../../common/api-service/api.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable, Observer, Subscription } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-a1lop',
  templateUrl: './a1lop.component.html',
  styleUrls: ['./a1lop.component.scss'],
})
export class A1lopComponent implements OnInit, OnDestroy {
  /** for table */
  displayedColumns: string[] = [
    'select',
    'name',
    'address',
    'createdate',
    'amount',
    'price',
    'edit',
  ];

  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  selection = new SelectionModel<any>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    if (this.dataSource) {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
    return null;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.position + 1
    }`;
  }
  // end for table;

  //subscription
  subscription: Subscription[] = [];

  // get role
  staff = {
    email: '',
    role: '',
  };

  //data permission
  isPermissionMenu1: boolean = false;

  // list class
  listClass: any[];

  /**
   * constructor
   * @param api
   * @param dialog
   */
  constructor(private api: ApiService, public dialog: MatDialog) {}

  /**
   * ngOnInit
   */
  ngOnInit() {
    // getList Class
    this.getListClass();
    this.staff = this.api.staffSubject.value;
    this.onLoadPermission();
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * on Load Permission
   */
  onLoadPermission() {
    // load permission
    if (this.staff.role.search('a:3') >= 0) {
      this.isPermissionMenu1 = true;
    }
  }

  /**
   * get List Class
   */
  getListClass() {
    const param = {};
    this.subscription.push(
      this.api.excuteAllByWhat(param, '100').subscribe((data) => {
        if (data) {
          // set data for table
          this.listClass = data;
          this.dataSource = new MatTableDataSource(data);
        } else {
          this.dataSource = new MatTableDataSource([]);
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.selection = new SelectionModel<any>(true, []);
      })
    );
  }

  /**
   * On Accept Click
   */
  onAcceptClick() {
    // get listId selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach((item) => {
      if (listId == '') {
        listId = item.id;
      } else {
        listId += ',' + item.id;
      }
    });

    const param = { listid: listId, statusid: '1' };

    // start single approval
    if (listId != '') {
      this.subscription.push(
        this.api.excuteAllByWhat(param, 'xxx').subscribe((data) => {
          this.api.showSuccess('Duyệt thành công ');
        })
      );
    } else {
      this.api.showWarning('Vui lòng chọn ít nhất một mục để duyệt ');
    }
  }

  /**
   * on Delete Click
   */
  onBtnDelClick() {
    // get listId selection example: listId='1,2,6'
    let listId = '';
    this.selection.selected.forEach((item) => {
      if (listId == '') {
        listId += item.id;
      } else {
        listId += ',' + item.id;
      }
    });

    const param = { listid: listId };

    // start delete
    if (listId !== '') {
      this.subscription.push(
        this.api.excuteAllByWhat(param, '103').subscribe((data) => {
          // load data grid
          this.getListClass();

          //scroll top
          window.scroll({ left: 0, top: 0, behavior: 'smooth' });

          // show toast success
          this.api.showSuccess('Xóa thành công ');
        })
      );
    } else {
      // show toast warning
      this.api.showWarning('Vui lòng chọn 1 mục để xóa ');
    }
    this.selection = new SelectionModel<any>(true, []);
  }

  /**
   * on insert data
   * @param event
   */
  onInsertData() {
    const dialogRef = this.dialog.open(A1lopDialog, {
      width: '700px',
      data: { type: 0, id: 0 },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getListClass();
      }
    });
  }

  /**
   * on update data
   * @param event
   */
  onUpdateData(row) {
    const dialogRef = this.dialog.open(A1lopDialog, {
      width: '700px',
      data: { type: 1, input: row },
      panelClass: 'custom-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getListClass();
      }
    });
  }
}

/**
 * format date
 */
export const MY_FORMATS = {
  parse: {
    dateInput: 'DD-MM-YYYY',
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DDDD',
    monthYearA11yLabel: 'MM YYYY',
  },
}

/**
 * Component show thông tin để insert or update
 */
@Component({
  selector: 'a1lop-dialog',
  templateUrl: 'a1lop-dialog.html',
  styleUrls: ['./a1lop.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class A1lopDialog implements OnInit, OnDestroy {
  observable: Observable<any>;
  observer: Observer<any>;

  //type
  type: number;

  //subscription
  subscription: Subscription[] = [];

  // init input value
  input: any = {
    name: '',
    address: '',
    createdate: '',
    amount: '',
    price: '',
  };

  date = new FormControl(moment());
  form: FormGroup;

  /**
   * contructor
   * @param dialogRef
   * @param data
   * @param api
   * @param formBuilder
   */
  constructor(
    public dialogRef: MatDialogRef<A1lopDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private api: ApiService,
    private formBuilder: FormBuilder
  ) {
    this.type = data.type;

    // if update
    if (this.type == 1) {
      this.input = data.input;
    }

    // add validate for controls
    this.form = this.formBuilder.group({
      name: [
        null,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(100),
        ],
      ],
      address: [
        null,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(255),
        ],
      ],
      createdate: [null],
      amount: [null, [Validators.required, Validators.maxLength(100)]],
      price: [
        null,
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(255),
        ],
      ],
    });

    // xử lý bất đồng bộ
    this.observable = Observable.create((observer: any) => {
      this.observer = observer;
    });
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    //scroll top
    window.scroll({ left: 0, top: 0, behavior: 'smooth' });
  }

  /**
   * ngOnDestroy
   */
  ngOnDestroy() {
    this.subscription.forEach((item) => {
      item.unsubscribe();
    });
  }

  /**
   * on Button click
   */
  onBtnClick(): void {
    //check validation
    if (this.form.status != 'VALID') {
      this.api.showWarning('Vui lòng nhập các mục đánh dấu * ');
      return;
    } else {
      //if type = 0 => Insert data else update data
      this.subscription.push(
        this.api
          .excuteAllByWhat(this.input, '' + Number(101 + this.type) + '')
          .subscribe((data) => {
            this.dialogRef.close(true);
            this.api.showSuccess('Xử Lý Thành Công ');
          })
      );
    }
  }
}


