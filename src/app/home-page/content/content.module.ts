import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentComponent } from './content.component';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { ApiService } from '../../common/api-service/api.service';
import { IMyProfileModule } from './i-my-profile/i-my-profile.module';
import { ChangePassworkModule } from './change-password/change-password.module';

@NgModule({
  declarations: [ContentComponent],
  imports: [
    TransferHttpCacheModule,
    CommonModule,
    RouterModule.forChild([
      {
        // ng g module home-page/content/sellTicket --module content
        // ng g c home-page/content/sellTicket
        path: '',
        component: ContentComponent,
        children: [
          {
            path: 'home',
            loadChildren: () =>
              import('./home/home.module').then((m) => m.HomeModule),
          },
          {
            path: 'a1-lop',
            loadChildren: () =>
              import('./a1-lop/a1lop.module').then((m) => m.A1lopModule),
          },
          {
            path: 'b1-user',
            loadChildren: () =>
              import('./b1-user/b1user.module').then((m) => m.B1userModule),
          },
          {
            path: 'c1-staff',
            loadChildren: () =>
              import('./c1-staff/c1staff.module').then((m) => m.C1staffModule),
          },
          {
            path: 'home',
            loadChildren: () =>
              import('./home/home.module').then((m) => m.HomeModule),
          },
          {
            path: 'my-profile',
            loadChildren: () =>
              import('./i-my-profile/i-my-profile.module').then(
                (m) => m.IMyProfileModule
              ),
          },
          {
            path: 'change-password',
            loadChildren: () =>
              import('./change-password/change-password.module').then(
                (m) => m.ChangePassworkModule
              ),
          },
          {
            path: 'home',
            loadChildren: () =>
              import('./home/home.module').then((m) => m.HomeModule),
          },
        ],
      },
    ]),
    IMyProfileModule,
    ChangePassworkModule,
  ],
  providers: [ApiService],
  entryComponents: [],
})
export class ContentModule {}
