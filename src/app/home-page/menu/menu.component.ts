import { Component, OnInit } from '@angular/core';
import { LoginCookie } from '../../common/core/login-cookie';
import { ApiService } from 'src/app/common/api-service/api.service';
// import $ from 'jquery';
import * as $ from 'jquery';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  //menu flag
  menuFlag: boolean = false;
  menuMobileFlag: boolean = false;
  menuMobileStaff: boolean = false;
  settingButton: boolean = false;
  searchFlag: boolean = false;
  isMobile: boolean = false;
  idStaff: Number;

  //data permission menu
  isPermissionMenu1: boolean = false;
  isPermissionMenu2: boolean = false;
  isPermissionMenu3: boolean = false;

  // show dropdown (logout/myprofile/changepasswork)
  isShow: boolean = false;
  info: any = null;

  //pubic is visited
  public isVisited = false;
  public checkVisited() {
    // reverse the value of property
    this.isVisited = !this.isVisited;
  }

  //data binding
  staff = {
    email: '',
    name: '',
    role: '',
    img: '',
  };

  /**
   * constructor
   * @param login
   * @param api
   */
  constructor(private login: LoginCookie, public api: ApiService) {
    // get staff value
    this.staff = this.api.getStaffValue;
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.staff = this.api.staffSubject.value;
    this.idStaff = this.api.staffSubject.value.id;
    this.isMobile = this.isMobileDevice();
    this.onLoadPermission();

    // toggle Sidebar Click
    this.toggleSidebarClick();
  }


  /**
   * onLoadPermission
   */
  onLoadPermission() {
    // load permission
    if (this.staff.role.search('a:1') < 0) {
      this.isPermissionMenu1 = true;
    }
    if (this.staff.role.search('b:1') < 0) {
      this.isPermissionMenu2 = true;
    }
    if (this.staff.role.search('c:1') < 0) {
      this.isPermissionMenu3 = true;
    }
  }

  /**
   * is Mobile Device
   */
  isMobileDevice() {
    return (
      typeof window.orientation !== 'undefined' ||
      navigator.userAgent.indexOf('IEMobile') !== -1
    );
  }

  /**
   * onBtnLogOutStaffClick
   */
  onBtnLogOutStaffClick() {
    this.api.logoutStaff();
  }

  /**
   * logOut Staff
   */
  toggleSidebarClick() {
    $(document).ready(function () {
      $(".icon-toggle-sidebar").click(function () {
        $("html body").toggleClass("sidebar-icon-only");
      });
    });
  }
}
